init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);

    const options = Engine.ReadJSONFile("gui/options/options~QuickStart.json");
    g_Options = g_Options.concat(options);

    // TODO: translation. The expected command (below) raises an error
    // translateObjectKeys(g_Options, ["label", "tooltip"]);

    placeTabButtons(
	g_Options,
	false,
	g_TabButtonHeight,
	g_TabButtonDist,
	selectPanel,
	displayOptions
    );

    // Write default values if missing
    const prefix = "quickstart.";
    let settings = {};
    options.forEach(category => category.options.forEach(option => settings[option.config] = ("val" in option) ? option.val.toString() : rgbToGuiColor(defaultColors[+option.config.substring(prefix.length)])));
    Object.keys(settings).filter(key => !Engine.ConfigDB_GetValue("user", key)).forEach(key => Engine.ConfigDB_CreateValue("user", key, settings[key]));
    Engine.ConfigDB_SaveChanges("user");

}});

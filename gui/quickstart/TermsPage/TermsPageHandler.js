class QuickStartTermsPage
{

    constructor()
    {
        // GUI objects
        this.titleLabel = Engine.GetGUIObjectByName("titleLabel");
        this.textLabel = Engine.GetGUIObjectByName("textLabel");
        this.showAgainLabel = Engine.GetGUIObjectByName("showAgainLabel");
        this.showAgainButton = Engine.GetGUIObjectByName("showAgainButton");
        this.confirmButton = Engine.GetGUIObjectByName("confirmButton");

        // Events
        this.confirmButton.onPress = this.onPressConfirmButton.bind(this);

        this.init();
    }

    onPressConfirmButton()
    {
        const showAgain = this.showAgainButton.checked;
        if (!showAgain)
        {
            Engine.ConfigDB_CreateValue("user", "quickstart.showterms", false);
            Engine.ConfigDB_SaveChanges("user");
        }
        Engine.PopGuiPage();
    }

    init(data)
    {
        this.titleLabel.caption = translate("QuickStart mod");
        this.textLabel.caption = translate("QuickStart mod executes automated actions. This can be considered as a cheat.") + "\n\n" +
            translate("When you play a game with this mod, make sure other players are aware you are using this mod and agree.");
        this.showAgainLabel.caption = translate("Show this message in the future");
        this.confirmButton.caption = translate("I understand");
    }

}

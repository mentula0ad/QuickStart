class QuickStart
{

    constructor()
    {
        this.settings;
        this.playerEntities;
        this.civilCentreUnitAssociation;
    }

    // -------
    // Getters
    // -------

    getSettings()
    {
        const options = Engine.ReadJSONFile("gui/options/options~QuickStart.json");
        const prefix = "quickstart.";
        let settings = {};
        options.forEach(category => category.options.forEach(option => settings[option.config.substring(prefix.length)] = option.val.toString()));
        for (const key in settings)
        {
            const value = Engine.ConfigDB_GetValue("user", prefix + key);
            if (value.length > 0)
                settings[key] = value;
        }

        // Translate values into labels
        options.forEach(category => category.options.forEach(option => settings[option.config.substring(prefix.length)] = option.list.find(x => x.value == parseInt(settings[option.config.substring(prefix.length)])).label));

        return settings;
    }

    getPlayerEntities()
    {
        let playerEntities = {
            "CivilCentre": [],
            "FemaleCitizen": [],
            "Infantry": [],
            "Cavalry": []
        };

        const interfacePlayerEntities = Engine.GuiInterfaceCall("GetPlayerEntities");
        for (const entityId of interfacePlayerEntities)
        {
            const state = GetEntityState(entityId);
            const entityClass = Object.keys(playerEntities).find(x => hasClass(state, x));
            if (!!entityClass)
                playerEntities[entityClass].push(entityId);
        }

        return playerEntities;
    }

    getGarrisonableEntities()
    {
        return [
            ...this.playerEntities.FemaleCitizen,
            ...this.playerEntities.Infantry,
            ...this.playerEntities.Cavalry
        ];
    }

    getGaiaEntities(resourceType)
    {
        // If no resource has been set, return an empty list
        if (!resourceType.generic || !resourceType.specific)
            return [];

        const interfaceGaiaEntities = Engine.GuiInterfaceCall("GetGaiaEntities");
        return interfaceGaiaEntities.filter(x => GetEntityState(x).resourceSupply?.type?.specific === resourceType.specific)
    }

    getCivilCentreUnitAssociation()
    {
        let civilCentreUnitAssociation = Object.fromEntries(this.playerEntities.CivilCentre.map(id => [id, []]));
        const garrisonable = this.getGarrisonableEntities();
        for (const id of garrisonable)
        {
            const closestCivilCentre = this.getClosestEntity(id, this.playerEntities.CivilCentre);
            if (closestCivilCentre === undefined)
                continue;
            civilCentreUnitAssociation[closestCivilCentre].push(id);
        }
        return civilCentreUnitAssociation;
    }

    getUnitResourceAssociation()
    {
        return {
            FemaleCitizen: this.getResourceObject(this.settings.female),
            Infantry: this.getResourceObject(this.settings.infantry),
            Cavalry: this.getResourceObject(this.settings.cavalry)
        };
    }

    getResourceObject(label)
    {
        if (label == "Fruit")
            return {"generic": "food", "specific": "fruit"};
        if (label == "Meat")
            return {"generic": "food", "specific": "meat"};
        if (label == "Wood")
            return {"generic": "wood", "specific": "tree"};
        if (label == "Stone")
            return {"generic": "stone", "specific": "rock"};
        if (label == "Metal")
            return {"generic": "metal", "specific": "ore"};
        return {"generic": undefined, "specific": undefined};
    }

    getClosestEntity(entityId, entityList)
    {
        // Get info on given entity
        const state = GetEntityState(entityId);
        const position = Vector2D.from3D(state.position);

        // Initialize candidate target
        let closestCandidate = undefined;
        let distanceCandidate = Infinity;

        // Find closest candidate target
        for (const id of entityList)
        {
            const targetState = GetEntityState(id);
            const targetPosition = Vector2D.from3D(targetState.position);
            const distance = position.distanceTo(targetPosition);
            if (distance < distanceCandidate && targetState.visibility != "hidden")
            {
                distanceCandidate = distance;
                closestCandidate = id;
            }
        }

        return closestCandidate;
    }

    // -------
    // Helpers
    // -------

    setRallyPoint(entityId, resourceType)
    {
        const closestResource = this.getClosestEntity(entityId, this.getGaiaEntities(resourceType));

        // Simply unset rally point if no target resource has been found
        if (!closestResource)
        {
            g_UnitActions["unset-rallypoint"].execute(
                undefined,
                undefined,
                [entityId],
                false,
                false
            );
            return;
        }

        const closestResourceState = GetEntityState(closestResource);
        const closestResourcePosition = closestResourceState.position;
        const closestResourceTemplate = closestResourceState.template;
        g_UnitActions["set-rallypoint"].execute(
            closestResourcePosition,
            {"data": {
                "command": "gather",
                "resourceType": resourceType,
                "resourceTemplate": closestResourceTemplate,
                "target": closestResource
            }},
            [entityId],
            false,
            false
        );
    }

    unloadUnitClassFromBuilding(entityId, unitClass)
    {
        const unitList = this.playerEntities[unitClass];
        const templates = new Set(unitList.map(id => GetTemplateData(GetEntityState(id).template).selectionGroupName));
        for (const template of templates.keys())
        {
            Engine.PostNetworkCommand({
	        "type": "unload-template",
	        "all": true,
	        "template": template,
	        "owner": g_ViewedPlayer,
	        "garrisonHolders": [entityId]
	    });
        }
    }

    // --------------------------------
    // Actions that determine the state
    // --------------------------------

    isTooLate()
    {
        return this.currentState == "START" && g_SimState.timeElapsed > this.tooLateToExecute;
    }

    isStartComplete()
    {
        if (this.currentState != "START")
            return false;
        return g_SimState.timeElapsed > 0;
    }

    isGarrisonComplete()
    {
        if (this.currentState != "BEFORE_UNLOAD")
            return false;
        return this.playerEntities.CivilCentre.every(id => GetEntityState(id).garrisonHolder.entities.length == this.civilCentreUnitAssociation[id].length);
    }

    isUnloadComplete()
    {
        if (this.currentState != "UNLOADING")
            return false;
        return this.playerEntities.CivilCentre.every(id => GetEntityState(id).garrisonHolder.entities.length == 0);
    }

    nextState()
    {
        const index = this.states.indexOf(this.currentState);
        this.currentState = this.states[index+1];
    }

    // ---------------------------------------------------
    // Actions to be executed after game start is complete
    // ---------------------------------------------------

    init()
    {
        this.settings = this.getSettings();
        this.playerEntities = this.getPlayerEntities();
        this.civilCentreUnitAssociation = this.getCivilCentreUnitAssociation();
    }

    garrison()
    {
        for (const civilCentreId of Object.keys(this.civilCentreUnitAssociation).map(x => +x))
        {
            g_UnitActions["garrison"].execute(
                civilCentreId,
                {"target": civilCentreId},
                this.civilCentreUnitAssociation[civilCentreId],
                false,
                false
            );
        }
    }

    trainUnits()
    {
        if (this.settings.train == "None")
            return;

        if (this.playerEntities.CivilCentre.length == 0)
            return;

        // This could possibly change: 0 => Female, 1 => Melee infantry, etc.
        const index = [
            "Female citizen",
            "Melee infantry",
            "Ranged infantry",
            "Cavalry"
        ].indexOf(this.settings.train);
        const template = GetEntityState(this.playerEntities.CivilCentre[0]).trainer.entities[index];
        Engine.PostNetworkCommand({
	    "type": "train",
            "entities": this.playerEntities.CivilCentre,
	    "template": template,
            "count": parseInt(this.settings.batch),
            "pushFront": false
	});
    }

    // -------------------------------------------------
    // Actions to be executed after garrison is complete
    // -------------------------------------------------

    unload()
    {
        const unitResourceAssociation = this.getUnitResourceAssociation();
        for (const entityId of this.playerEntities.CivilCentre.map(x => +x))
        {
            for (const unitClass in unitResourceAssociation)
            {
                const resourceType = unitResourceAssociation[unitClass];
                this.setRallyPoint(entityId, resourceType);
                this.unloadUnitClassFromBuilding(entityId, unitClass)
            }
        }
    }

    // -----------------------------------------------
    // Actions to be executed after unload is complete
    // -----------------------------------------------

    selectUnits()
    {
        let key = this.settings.select;
        if (key == "None")
            return;
        if (key == "Female citizen")
            key = "FemaleCitizen";
        g_Selection.reset();
        g_Selection.addList(this.playerEntities[key]);
    }

    placeFoundation()
    {
        let key = this.settings.foundation;
        if (key == "None")
            return;
        key = key.toLowerCase();
        const playerState = GetSimState().players[Engine.GetPlayerID()];
        const template = "structures/" + playerState.civ + "/" + key;
        startBuildingPlacement(template, playerState);
    }

    moveCamera()
    {
        if (this.playerEntities.CivilCentre.length == 0)
            return;

        const entityId = this.playerEntities.CivilCentre[0];
        const resourceType = this.getResourceObject(this.settings.camera);
        const closestResource = this.getClosestEntity(entityId, this.getGaiaEntities(resourceType));
        const closestResourceState = GetEntityState(closestResource);
        const closestResourcePosition = closestResourceState.position;
        Engine.CameraMoveTo(closestResourcePosition.x, closestResourcePosition.z);
    }

    setFinalRallyPoint()
    {
        const resourceType = this.getResourceObject(this.settings.rally);
        for (const entityId of this.playerEntities.CivilCentre.map(x => +x))
            this.setRallyPoint(entityId, resourceType);
    }

}

QuickStart.prototype.states = ["START", "BEFORE_UNLOAD", "UNLOADING"];

QuickStart.prototype.currentState = "START";

QuickStart.prototype.tooLateToExecute = 1000;

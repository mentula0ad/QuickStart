var g_QuickStart;

init = new Proxy(init, {apply: function(target, thisArg, args) {
    target(...args);

    if (!g_InitAttributes.settings.Nomad)
        g_QuickStart = new QuickStart();
}});

onTick = new Proxy(onTick, {apply: function(target, thisArg, args) {
    target(...args);

    if (!global.g_QuickStart)
        return;

    if (g_QuickStart.isTooLate())
    {
        g_QuickStart = null;
        return;
    }

    if (g_QuickStart.isStartComplete())
    {
        try
        {
            g_QuickStart.init();
            g_QuickStart.garrison();
            g_QuickStart.trainUnits();
            g_QuickStart.nextState();
        }
        catch (error) { g_QuickStart = null }
        finally { return }
    }

    if (g_QuickStart.isGarrisonComplete())
    {
        try
        {
            g_QuickStart.unload();
            g_QuickStart.nextState();
        }
        catch (error) { g_QuickStart = null }
        finally { return }
    }

    if (g_QuickStart.isUnloadComplete())
    {
        try
        {
            g_QuickStart.selectUnits();
            g_QuickStart.placeFoundation();
            g_QuickStart.moveCamera();
            g_QuickStart.setFinalRallyPoint();
            g_QuickStart = null;
        }
        catch (error) { g_QuickStart = null }
        finally { return }
    }
}});

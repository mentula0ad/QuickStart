# About QuickStart

*QuickStart* is a [0 A.D.](https://play0ad.com) mod that allows to automatically perform predefined actions as game starts, reducing initial micromanagement.

**WARNING:** this mod executes automated actions. This can be considered as a cheat. When you play a game with this mod, make sure other players are aware you are using this mod and agree.

# Game start actions

- Garrison all units and unload them to gather specific resources.
- Train new units.
- Select specific units and prepare the foundation of a building.
- Jump with camera on specific resources.

# Installation

[Click here](https://gitlab.com/mentula0ad/QuickStart/-/releases/permalink/latest/downloads/quickstart.pyromod) to download the latest release. Install following the official 0 A.D. guide: [How to install mods?](https://trac.wildfiregames.com/wiki/Modding_Guide#Howtoinstallmods)

_Alternative downloads:_ [Latest Release (.pyromod)](https://gitlab.com/mentula0ad/QuickStart/-/releases/permalink/latest/downloads/quickstart.pyromod) | [Latest Release (.zip)](https://gitlab.com/mentula0ad/QuickStart/-/releases/permalink/latest/downloads/quickstart.zip) | [Older Releases](https://gitlab.com/mentula0ad/QuickStart/-/releases)

# Questions & feedback

For more information, questions and feedback, visit the [thread on the 0 A.D. forum](https://wildfiregames.com/forum/topic/107436-quickstart-mod-automate-repeated-actions-at-game-start/).

GuiInterface.prototype.ScriptCall = new Proxy(GuiInterface.prototype.ScriptCall, {apply: function(target, thisArg, args) {
    if (args[1] == "GetGaiaEntities")
        return Engine.QueryInterface(SYSTEM_ENTITY, IID_RangeManager).GetEntitiesByPlayer(0);
    return target.apply(thisArg, args);
}});

Engine.ReRegisterComponentType(IID_GuiInterface, "GuiInterface", GuiInterface);
